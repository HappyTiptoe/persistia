# persistia

> Persist [Pinia](https://pinia.vuejs.org) state data in localStorage

<a href="https://npmjs.com/package/persistia">
  <img src="https://img.shields.io/npm/v/persistia" alt="npm version">
</a>
<a href="https://npmjs.com/package/persistia">
  <img src="https://img.shields.io/npm/l/persistia" alt="license">
</a>

## Installation

Install with your favourite package manager:

```sh
# yarn
yarn add persistia

# pnpm
pnpm add persistia

# npm
npm install persistia
```

Import and install:

```ts
// main.js
import { createPinia } from 'pinia'
import { persistia } from 'persistia'

const pinia = createPinia()
pinia.use(persistia)
```

Enable the plugin per store:

```ts
import { defineStore } from 'pinia'

export const useStore = defineStore('...', {
  persist: true,

  state: () => ({ ... }),

  actions: { ... },

  getters: { ... }
})
```

Alternatively, you can specify which keys in `state` to persist:

```ts
import { defineStore } from 'pinia'

export const useStore = defineStore('...', {
  persist: ['name', 'age'],

  state: () => ({
    name: 'Jane Doe',
    age: 42,
    email: 'jane@example.com',
    address: '123 Fake St.'
  }),

  actions: { ... },

  getters: { ... }
})
```

## License

[MIT](http://opensource.org/licenses/MIT)
