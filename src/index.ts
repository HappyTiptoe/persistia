import { PiniaPluginContext } from 'pinia'
import { Obj, isArray, isBrowser } from 'tsu'
import { watch } from 'vue'

declare module 'pinia' {
  interface DefineStoreOptionsBase<S extends StateTree, Store> {
    persist?: boolean | string[]
  }
}

function persistState(storeId: string, state: Obj<any>) {
  const persistedState = JSON.parse(localStorage.getItem(storeId) || '{}')

  const newState = { ...persistedState, ...state }

  localStorage.setItem(storeId, JSON.stringify(newState))
}

export function persistia({ options, store }: PiniaPluginContext) {
  if (options?.persist && isBrowser()) {
    const persistedState = localStorage.getItem(store.$id)

    // patch the persisted state into the store
    if (persistedState) {
      store.$patch(JSON.parse(persistedState))
    }

    const persistKeys = isArray(options.persist) ? options.persist : Object.keys(store.$state)

    watch(
      () => store.$state,
      (newState) => {
        const stateToPersist: Obj<any> = {}

        for (const key of persistKeys) {
          stateToPersist[key] = newState[key]
        }

        persistState(store.$id, stateToPersist)
      },
      { deep: true }
    )
  }
}
